//
//  CDMutualGroup+CoreDataClass.swift
//  ChatApplication
//
//  Created by hamed on 1/8/23.
//
//

import CoreData
import Foundation

@objc(CDMutualGroup)
public final class CDMutualGroup: NSManagedObject {}
