//
//  CDForwardInfo+CoreDataClass.swift
//  ChatApplication
//
//  Created by hamed on 1/8/23.
//
//

import CoreData
import Foundation

@objc(CDForwardInfo)
public final class CDForwardInfo: NSManagedObject {}
